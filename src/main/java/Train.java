import lombok.Data;

import java.util.ArrayList;
import java.util.Date;

@Data
public class Train {

    private Date appearanceTime;

    private ArrayList<Signal> signals;

    Train(Signal signal, Date appearanceTime){
        signals = new ArrayList<>();
        signals.add(signal);

        this.appearanceTime = appearanceTime;
    }
}
