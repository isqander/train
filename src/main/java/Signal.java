import lombok.Data;

@Data
public class Signal {
    private String uid;
    private double timestamp;
    private String antenna;
}
