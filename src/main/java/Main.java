import com.google.gson.Gson;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static spark.Spark.*;

public class Main {
    final static int TIME_TO_LEAVE  = 10000;
    static Map<String, Train> trains = new ConcurrentHashMap<>();
    static int isEntry = 0;
    public static void main(String[] args) {
        port(9000);
        post("/event", (request, response) -> {
            Signal signal = new Gson().fromJson(request.body(), Signal.class);
            if (!trains.containsKey(signal.getUid())){
                trains.put(signal.getUid(),new Train(signal, new Date()));
            } else {
                trains.get(signal.getUid()).getSignals().add(signal);
            }
            response.status(200);
            return response.raw();
        });
        while (true){
            for (String trainUid : trains.keySet()){
                Train train = trains.get(trainUid);
                if (train.getAppearanceTime() != null){
                    if (new Date().getTime() - train.getAppearanceTime().getTime() > TIME_TO_LEAVE){
                        train.getSignals().sort(Comparator.comparing(Signal::getTimestamp));
                        Double averageTimestamp = (train.getSignals().get(0).getTimestamp() + train.getSignals().get(train.getSignals().size()-1).getTimestamp())/2;
                        train.getSignals().forEach(signal -> {
                            if (signal.getTimestamp() < averageTimestamp){
                                switch (signal.getAntenna()){
                                    case "1": increaseIsEntry();
                                    case "2": decreaseIsEntry();
                                    case "3": increaseIsEntry();
                                    case "4": decreaseIsEntry();
                                }
                            } else {
                                switch (signal.getAntenna()){
                                    case "1": decreaseIsEntry();
                                    case "2": increaseIsEntry();
                                    case "3": decreaseIsEntry();
                                    case "4": increaseIsEntry();
                                }
                            }
                        });
                        if (isEntry > 0){
                            System.out.println("Въезд " + trainUid);
                        } else {
                            System.out.println("Выезд " + trainUid);
                        }
                        trains.remove(trainUid);
                    }
                }
            }
        }
    }

    static void increaseIsEntry(){
        isEntry++;
    }

    static void decreaseIsEntry(){
        isEntry--;
    }

}
